import random
import string


class DNA:

    def __init__(self, length):
        self.length = length
        self.fitness = 0
        self.genes = []

        for i in range(self.length):
            self.genes.append(random.choice(string.ascii_lowercase + ' '))

            # print(self.genes)

    def calc_fitness(self, target):
        score = 0
        for idx, gene in enumerate(self.genes):
            if gene == target[idx]:
                score += 1

        self.fitness = score

    def crossover(self, partner):
        midpoint = self.length // 2
        child = DNA(self.length)
        for i in range(self.length):
            if i > midpoint:
                child.genes[i] = self.genes[i]
            else:
                child.genes[i] = partner.genes[i]

        return child

    def mutate(self, mutation_rate):
        k = 0
        for idx in range(self.length):
            k = random.uniform(0, 1)
            if k < mutation_rate:
                self.genes[idx] = random.choice(string.ascii_lowercase + ' ')


