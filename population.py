from os import system
from dna import DNA
import random

class Population:

    def __init__(self, amount, target, mutation):
        self.amount = amount
        self.target = target
        self.mutation = mutation

        self.population = []

        self.mating_pool = []

        self.is_done = False

        self.best = ''
        self.generations = 0

        for i in range(self.amount):
            dna = DNA(len(self.target))
            self.population.append(dna)

        self.calc_fitness()

    def calc_fitness(self):
        for dna in self.population:
            dna.calc_fitness(self.target)

    def make_selection(self):
        self.mating_pool = []
        total = 0
        for dna in self.population:
            total += dna.fitness

        coef = 100 / total
        for dna in self.population:
            accurances = round(coef * dna.fitness)
            for i in range(accurances):
                self.mating_pool.append(dna)

    def evaluate(self):
        best = DNA(len(self.target))
        best.genes = []

        for dna in self.population:
            if best.fitness < dna.fitness:
                best = dna

        self.best = ''.join(best.genes)

        if self.best == self.target:
            self.is_done = True


    def generate(self):
        for i in range(self.amount):
            A = random.choice(self.mating_pool)
            B = random.choice(self.mating_pool)
            child = A.crossover(B)
            child.mutate(self.mutation)
            self.population[i] = child
        self.generations += 1

    def show_status(self):
        system("clear")
        print(f"Best phrase: {self.best}")
        print(f"Target: {self.target}")
        print(f"Generations: {self.generations}")



