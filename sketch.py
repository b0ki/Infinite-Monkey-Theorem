from population import Population

amount = 200
target_phrase = "to be or not to be"
mutation_rate = 0.01

if __name__ == "__main__":

    population = Population(amount=amount, target=target_phrase, mutation=mutation_rate)

    while True:
        population.make_selection()
        population.generate()
        population.calc_fitness()
        population.evaluate()

        population.show_status()

        if population.is_done:
            break